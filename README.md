# Dockerpipe

Dockerpipe is a simple ansible role that pulls a repository that contains an application, a `Dockerfile` and a `docker-compose.yml`. It builds the `Dockerfile` into an Image, starts the application container with `docker-compose` and pushes the Image into a dockerhub repository defined by the user.

The tasks of the role recieve three variables that can be passed as arguments in the `ansible-playbook` call. This are the repository to pull, the directory in which to clone the repository, and the dockerhub repository to push the built Image. This are further explained in the README.md of the role.

It's important to mention that, for testing purposes, the `inventory.txt` defines a localhost that's hardcoded into the `playbook.yml` host's. In case you would like to use the `localhost` as your host remember to pass the ansible `ssh-key` with `ssh-copy-id localhost`.

## Installation

Clone this repository into the Ansible Controller machine.

## Usage

Clone this repository in a machine that has `git`, `ansible` and `docker` installed and set up. Or add a task before every other task that install those pkgs using the package manager of your distribution. Since I can't know the machine you will be testing this on.

Inside of your cloned repository run the next command (*For testing purposes we are going to use a hello-world NodeJS application*) :

```bash
ansible-playbook playbook.yml -e "repo_url=https://gitlab.com/rivera-bl/hello-node.git clone_dir=<dir-fullpath> dockerhub_repo=<username/repository>" -i inventory.txt
```

Note: The ansible role creates the folder you specify in `clone_dir`, there's no need to create it in advance.

If you now go to `http://localhost:8081` you should see the *hello world* page

## Considerations

The NodeJS application uses a package called `forever`, which continuosly executes the application. `forever` also saves it's log inside an specific folder. In order to keep this information after the Docker Container is terminated, this log folder is saved as a `volume` in the folder defined by `clone_dir`.

## License
[MIT](https://choosealicense.com/licenses/mit/)
