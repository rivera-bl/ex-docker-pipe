DOCKERPIPE
=========

Simple role for pulling a git repository that contains a Dockerfile and a docker-compose.yaml. Ansible builds the Dockerfile and pushes the Image to a Docker Repository defined by the user. Finally, it runs the docker-compose.yaml for starting the applicatioin

Requirements
------------

git, docker & docker-compose for using the git, docker_image and docker_compose module.

Role Variables
--------------

This Role has two important variables: **repo_url**, **clone_dir** & **dockerhub_repo**. This variables can be set in the vars/main.yml file, or they can be passed as arguments in the ansible-playbook call for further flexibility. **repo_url** is the url of the repository to clone. **clone_dir** defines the directory where the git repository will be cloned, which is also the directory that docker_image and docker_compose will look for it's corresponding files. While **dockerhub_repo**, defines the repository in which to push the built image.


Example Playbook
----------------

In your playbook file:

    - hosts: <your-host-defined-in-inventory.txt>
      roles:
         - dockerpipe

License
-------

BSD

Author Information
------------------

Made by Pablo Rojas Rivera for Odilo.
